from django.urls import path
from .views import _list_create

urlpatterns = [
    path("orders", _list_create),
]