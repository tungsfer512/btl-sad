from django.http import JsonResponse
from rest_framework.response import Response
from rest_framework import status
import requests
import os
import json
from rest_framework.decorators import api_view
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

user_id = openapi.Parameter(
    name="user_id",
    type=openapi.TYPE_INTEGER,
    in_=(openapi.IN_BODY, openapi.IN_QUERY, )
)
user_id_q = openapi.Parameter(
    name="user_id",
    type=openapi.TYPE_INTEGER,
    in_=openapi.IN_QUERY
)
# payment
payment_method = openapi.Parameter(
    name="payment_method",
    type=openapi.TYPE_STRING,
    in_=(openapi.IN_BODY, openapi.IN_QUERY, )
)
amount = openapi.Parameter(
    name="amount",
    type=openapi.TYPE_NUMBER,
    in_=(openapi.IN_BODY, openapi.IN_QUERY, )
)
card_number = openapi.Parameter(
    name="card_number",
    type=openapi.TYPE_STRING,
    in_=(openapi.IN_BODY, openapi.IN_QUERY, ),
    required=False
)
cvv = openapi.Parameter(
    name="cvv",
    type=openapi.TYPE_STRING,
    in_=(openapi.IN_BODY, openapi.IN_QUERY, ),
    required=False
)
payment_status = openapi.Parameter(
    name="payment_status",
    type=openapi.TYPE_STRING,
    in_=(openapi.IN_BODY, openapi.IN_QUERY, ),
    required=False
)
payment_status_q = openapi.Parameter(
    name="payment_status",
    type=openapi.TYPE_STRING,
    in_=openapi.IN_QUERY,
    required=False
)

# shipment
address_id = openapi.Parameter(
    name="address_id",
    type=openapi.TYPE_INTEGER,
    in_=(openapi.IN_BODY, openapi.IN_QUERY, )
)
shipment_method = openapi.Parameter(
    name="shipment_method",
    type=openapi.TYPE_STRING,
    in_=(openapi.IN_BODY, openapi.IN_QUERY, )
)
cost = openapi.Parameter(
    name="cost",
    type=openapi.TYPE_NUMBER,
    in_=(openapi.IN_BODY, openapi.IN_QUERY, )
)
note = openapi.Parameter(
    name="note",
    type=openapi.TYPE_STRING,
    in_=(openapi.IN_BODY, openapi.IN_QUERY, ),
    required=False
)
shipment_status = openapi.Parameter(
    name="shipment_status",
    type=openapi.TYPE_STRING,
    in_=(openapi.IN_BODY, openapi.IN_QUERY, ),
    required=False
)
shipment_status_q = openapi.Parameter(
    name="shipment_status",
    type=openapi.TYPE_STRING,
    in_=openapi.IN_QUERY,
    required=False
)

# products
cart_ids = openapi.Parameter(
    name="cart_ids",
    description="Get all products by categories if passing",
    type=openapi.TYPE_ARRAY,
    items=openapi.Items(type=openapi.TYPE_INTEGER),
    in_=openapi.IN_BODY
)

order_add_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT, 
    properties={
        'user_id': user_id,
        'cart_ids': cart_ids,
        'shipment_method': shipment_method,
        'address_id': address_id,
        'cost': cost,
        'note': note,
        'payment_method': payment_method,
        'card_number': card_number,
        'cvv': cvv,
    }
)

@swagger_auto_schema(
    method="post",
    request_body=order_add_schema
)
@swagger_auto_schema(
    method="get",
    manual_parameters=(user_id_q, shipment_status_q, payment_status_q)
)
@api_view(("GET", "POST", ))
def _list_create(request, *args, **kwargs):
    if request.method == "GET":
        try:
            params = request.query_params.dict()
            print(params)
            orders = json.loads(requests.get(f"{os.getenv('ORDER_SERVICE_ENDPOINT')}/orders", params=params).text).get("results")
            print(orders)
            for order in orders:
                for product in order.get("products"):
                    product_te = json.loads(requests.get(f"{os.getenv('PRODUCT_SERVICE_ENDPOINT')}/products/{product.get('product_id')}").text)
                    product["info"] = product_te
                shipment = json.loads(requests.get(f"{os.getenv('SHIPMENT_SERVICE_ENDPOINT')}/shipments/{order.get('shipment_id')}").text)
                order["shipment"] = shipment
                address = json.loads(requests.get(f"{os.getenv('USER_SERVICE_ENDPOINT')}/addresses/{shipment.get('address_id')}").text)
                order["address"] = address
                payment = json.loads(requests.get(f"{os.getenv('PAYMENT_SERVICE_ENDPOINT')}/payments/{shipment.get('payment_id')}").text)
                order["payment"] = payment
                
            return JsonResponse({
                "data": orders
            }, status=status.HTTP_200_OK)
        except Exception as error:
            return JsonResponse({
                "message": str(error)
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    elif request.method == "POST":
        try:
            data = request.data
            res = {}
            
            total = 0
            payment_status = None
            card_number = None
            cvv = None
            
            payment_method = data.get("payment_method")
            if payment_method == "cash":
                payment_status = "not_paid"
            else:
                payment_status = "paid"
                card_number = data.get("card_number")
                cvv = data.get("cvv")
            order_products_datas = []
            print(type(data.get("cart_ids")))
            for cart_id in data.get("cart_ids"):
                cart = json.loads(requests.get(f"{os.getenv('CART_SERVICE_ENDPOINT')}/carts/{cart_id}").text)
                product = json.loads(requests.get(f"{os.getenv('PRODUCT_SERVICE_ENDPOINT')}/products/{cart.get('product_id')}").text)
                total += float(product.get("price")) * int(cart.get("quantity"))
                te = {
                    "id": cart.get("product_id"),
                    "quantity": cart.get("quantity"),
                    "price": product.get("price"),
                }
                order_products_datas.append(te)
            print(order_products_datas)
            payment_data = {
                "user_id": data.get("user_id"),
                "payment_method": data.get("payment_method"),
                "amount": total + float(data.get("cost")),
                "payment_status": payment_status,
            }
            print(payment_data)
            if data.get("card_number", None) != None:
                payment_data["card_number"] = card_number
            payment = json.loads(requests.post(f"{os.getenv('PAYMENT_SERVICE_ENDPOINT')}/payments", json = payment_data).text)
            shipment_data = {
                "user_id": data.get("user_id"),
                "payment_id": payment.get("id"),
                "address_id": data.get("address_id"),
                "shipment_method": data.get("shipment_method"),
                "cost": data.get("cost"),
                "shipment_status": "waiting",
            }
            print(shipment_data)
            if data.get("note", None) != None:
                shipment_data["note"] = data.get("note")
            shipment = json.loads(requests.post(f"{os.getenv('SHIPMENT_SERVICE_ENDPOINT')}/shipments", json = shipment_data).text)
            order_data = {
                "user_id": data.get("user_id"),
                "shipment_id": shipment.get("id"),
                "products": order_products_datas
            }
            print(order_data)
            order = json.loads(requests.post(f"{os.getenv('ORDER_SERVICE_ENDPOINT')}/orders", json = order_data).text)
            print(order)
            order = json.loads(requests.get(f"{os.getenv('ORDER_SERVICE_ENDPOINT')}/orders/{order.get('id')}").text)
            print(order)
            for product in order.get("products"):
                product_te = json.loads(requests.get(f"{os.getenv('PRODUCT_SERVICE_ENDPOINT')}/products/{product.get('product_id')}").text)
                product["info"] = product_te
            address = json.loads(requests.get(f"{os.getenv('USER_SERVICE_ENDPOINT')}/addresses/{data.get('address_id')}").text)
            print(address)
            user = json.loads(requests.get(f"{os.getenv('USER_SERVICE_ENDPOINT')}/users/{data.get('user_id')}").text)
            print(user)
            for cart_id in data.get("cart_ids"):
                requests.delete(f"{os.getenv('CART_SERVICE_ENDPOINT')}/carts/{cart_id}")
            print(type(data.get("cart_ids")))
            res["user"] = user
            res["order"] = order
            res["shipment"] = shipment
            res["address"] = address
            res["payment"] = payment
            
            return JsonResponse({
                "data": res
            }, status=status.HTTP_201_CREATED)
        except Exception as error:
            return JsonResponse({
                "message": str(error)
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)