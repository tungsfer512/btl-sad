from django.urls import path
from .views import _list

urlpatterns = [
    path("comments", _list),
    # path("comments/<int:pk>", _retrieve_update_destroy),
]