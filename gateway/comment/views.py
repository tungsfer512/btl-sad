from django.http import JsonResponse
from rest_framework import status
import requests
import os
import json
from rest_framework.decorators import api_view
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema


user_id_b = openapi.Parameter(
    name="user_id",
    type=openapi.TYPE_INTEGER,
    in_=openapi.IN_BODY,
    required=False
)
order_id_b = openapi.Parameter(
    name="order_id",
    type=openapi.TYPE_INTEGER,
    in_=openapi.IN_BODY,
    required=False
)
product_id_b = openapi.Parameter(
    name="product_id",
    type=openapi.TYPE_INTEGER,
    in_=openapi.IN_BODY,
    required=False
)
rating_b = openapi.Parameter(
    name="rating",
    type=openapi.TYPE_STRING,
    in_=openapi.IN_BODY,
    required=False
)
content_b = openapi.Parameter(
    name="content",
    type=openapi.TYPE_STRING,
    in_=openapi.IN_BODY,
    required=False
)
user_id = openapi.Parameter(
    name="user_id",
    type=openapi.TYPE_INTEGER,
    in_=openapi.IN_QUERY,
    required=False
)
order_id = openapi.Parameter(
    name="order_id",
    type=openapi.TYPE_INTEGER,
    in_=openapi.IN_QUERY,
    required=False
)
product_id = openapi.Parameter(
    name="product_id",
    type=openapi.TYPE_INTEGER,
    in_=openapi.IN_QUERY,
    required=False
)
rating = openapi.Parameter(
    name="rating",
    type=openapi.TYPE_STRING,
    in_=openapi.IN_QUERY,
    required=False
)
content = openapi.Parameter(
    name="content",
    type=openapi.TYPE_STRING,
    in_=openapi.IN_QUERY,
    required=False
)
search = openapi.Parameter(
    name="search",
    type=openapi.TYPE_STRING,
    in_=openapi.IN_QUERY,
    required=False
)
page = openapi.Parameter(
    name="page",
    type=openapi.TYPE_INTEGER,
    in_=openapi.IN_QUERY,
    required=False
)
page_size = openapi.Parameter(
    name="page_size",
    type=openapi.TYPE_INTEGER,
    in_=openapi.IN_QUERY,
    required=False
)

add_comment_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT,
    properties={
        "user_id": user_id_b,
        "order_id": order_id_b,
        "product_id": product_id_b,
        "rating": rating_b,
        "content": content_b,
    }
)

@swagger_auto_schema(
    method="post",
    request_body=add_comment_schema
)
@swagger_auto_schema(
    method="get",
    manual_parameters=[search, user_id, order_id, product_id, rating, page, page_size]
)
@api_view(("GET", "POST", ))
def _list(request, *args, **kwargs):
    if request.method == "GET":
        try:
            params = request.query_params.dict()
            print(params)
            comments = json.loads(requests.get(f"{os.getenv('COMMENT_SERVICE_ENDPOINT')}/comments", params = params).text).get("results")
            return JsonResponse({
                "data": comments
            }, status=status.HTTP_200_OK)
        except Exception as error:
            return JsonResponse({
                "message": str(error)
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    elif request.method == "POST":
        try:
            data = request.data
            print(data)
            res = requests.post(f"{os.getenv('COMMENT_SERVICE_ENDPOINT')}/comments", json = data)
            comments = json.loads(res.text)
            if res.status_code == 201:
                return JsonResponse({
                    "data": comments
                }, status=status.HTTP_200_OK)
            else: 
                return JsonResponse({
                    "data": comments
                }, status=status.HTTP_400_BAD_REQUEST)
        except Exception as error:
            return JsonResponse({
                "message": str(error)
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

# @api_view(("delete", ))
# def _retrieve_update_destroy(request, *args, **kwargs):
#     if request.method == "DELETE":
#         try:
#             comment_id = int(kwargs.get("pk"))
#             comment = requests.delete(f"{os.getenv('COMMENT_SERVICE_ENDPOINT')}/comments/{comment_id}").text
#             return JsonResponse({
#                 "data": comment
#             }, status=status.HTTP_204_NO_CONTENT)
#         except Exception as error:
#             return JsonResponse({
#                 "message": str(error)
#             }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)