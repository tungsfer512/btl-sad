from rest_framework.permissions import BasePermission
import requests
import os
import json

def get_current_user_info(request, *args, **kwargs):
    headers = request.headers
    resp = requests.get(f"{os.getenv('USER_SERVICE_ENDPOINT')}/users/me", headers=headers)
    user = json.loads(resp.text)
    print(type(user))
    return user

class AllowAny(BasePermission):
    def has_permission(self, request, view):
        print("AllowAny")
        return True

class IsAuthenticated(BasePermission):
    def has_permission(self, request, view):
        print("IsAuthenticated")
        user = get_current_user_info(request)
        if type(user) is dict and "id" in user.keys():
            permission = True
            print(permission)
            return True
        return False

class IsStaff(BasePermission):
    def has_permission(self, request, view):
        print("IsStaff")
        user = get_current_user_info(request)
        if type(user) is dict and "id" in user.keys():
            permission = bool(user.get("is_staff"))
            print(permission)
            return permission
        return False

class IsAdmin(BasePermission):
    def has_permission(self, request, view):
        print("IsAdmin")
        user = get_current_user_info(request)
        if type(user) is dict and "id" in user.keys():
            permission = bool(user.get("is_staff") and user.get("is_superuser"))
            print(permission)
            return permission
        return False
