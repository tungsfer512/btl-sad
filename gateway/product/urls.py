from django.urls import path
from .views import _list_create, _retrieve_update_destroy

urlpatterns = [
    path("products", _list_create),
    path("products/<int:pk>", _retrieve_update_destroy),
]