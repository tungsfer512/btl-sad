from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from rest_framework import status
import requests
import os
import json
from rest_framework.decorators import api_view
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema


categories = openapi.Parameter(
    name="categories",
    description="Get all products by categories if passing",
    type=openapi.TYPE_ARRAY,
    items=openapi.Items(type=openapi.TYPE_INTEGER),
    in_=openapi.IN_QUERY
)
search = openapi.Parameter(
    name="search",
    description="Get all cart by search if passing",
    type=openapi.TYPE_STRING,
    in_=openapi.IN_QUERY
)
page = openapi.Parameter(
    name="page",
    type=openapi.TYPE_INTEGER,
    in_=openapi.IN_QUERY,
    required=False
)
page_size = openapi.Parameter(
    name="page_size",
    type=openapi.TYPE_INTEGER,
    in_=openapi.IN_QUERY,
    required=False
)
priceStart = openapi.Parameter(
    name="priceStart",
    type=openapi.TYPE_STRING,
    in_=openapi.IN_QUERY,
    required=False
)
priceEnd = openapi.Parameter(
    name="priceEnd",
    type=openapi.TYPE_STRING,
    in_=openapi.IN_QUERY,
    required=False
)

@swagger_auto_schema(
    method="get",
    manual_parameters=[search, categories, priceStart, priceEnd, page, page_size]
)
@api_view(("GET", ))
def _list_create(request, *args, **kwargs):
    if request.method == "GET":
        try:
            params = request.query_params.dict()
            print(params)
            category_params = params.get("categories", None)
            page = int(params.get("page", 1))
            page_size = int(params.get("page_size", 10))
            priceStart = params.get("priceStart", None)
            priceEnd = params.get("priceEnd", None)
            if category_params != None:
                categories = [int(i) for i in category_params]
                # categories = [int(i) for i in str(category_params).split(",")]
                del params["categories"]
                products = []
                for category in categories:
                    product_query_params = {
                        "category": category, 
                        "page": page, 
                        "page_size": page_size
                    }
                    if priceStart != None:
                        product_query_params["priceStart"] = priceStart
                    if priceEnd != None:
                        product_query_params["priceEnd"] = priceEnd
                    product_categories = json.loads(requests.get(f"{os.getenv('PRODUCT_SERVICE_ENDPOINT')}/products/categories", params = product_query_params).text).get("results")
                    for product_category in product_categories:
                        print(product_category)
                        product = json.loads(requests.get(f"{os.getenv('PRODUCT_SERVICE_ENDPOINT')}/products/{product_category.get('product')}").text)
                        product_images = json.loads(requests.get(f"{os.getenv('PRODUCT_SERVICE_ENDPOINT')}/products/images", params = {"product": product.get("id")}).text).get("results")
                        product["images"] = product_images
                        products.append(product)
                return JsonResponse({
                    "data": products
                }, status=status.HTTP_200_OK)
            else:
                products = json.loads(requests.get(f"{os.getenv('PRODUCT_SERVICE_ENDPOINT')}/products", params = params).text).get("results")
                for product in products:
                    product_categories = json.loads(requests.get(f"{os.getenv('PRODUCT_SERVICE_ENDPOINT')}/products/categories", params = {"product": product.get("id")}).text).get("results")
                    cat = []
                    for category in product_categories:
                        category_te = json.loads(requests.get(f"{os.getenv('PRODUCT_SERVICE_ENDPOINT')}/categories/{category.get('category')}").text)
                        cat.append(category_te)
                    product["categories"] = cat
                    product_images = json.loads(requests.get(f"{os.getenv('PRODUCT_SERVICE_ENDPOINT')}/products/images", params = {"product": product.get("id")}).text).get("results")
                    product["images"] = product_images
                return JsonResponse({
                    "data": products
                }, status=status.HTTP_200_OK)
            
            # if category_params != None:
            #     # del params["categories"]
            #     category_params = [int(i) for i in str(category_params).split(",")]
            #     te = []
            #     for ind in range(len(products)):
            #         category_product = [int(i.get("id")) for i in products[ind].get("categories")]
            #         check = any(item in category_product for item in category_params)
            #         if check == False:
            #             te.append(ind)
            #     te.sort(reverse=True)
            #     for i in te:
            #         products.pop(i)
        except Exception as error:
            return JsonResponse({
                "message": str(error)
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(("GET", ))
def _retrieve_update_destroy(request, *args, **kwargs):
    if request.method == "GET":
        try:
            product_id = int(kwargs.get("pk"))
            product = json.loads(requests.get(f"{os.getenv('PRODUCT_SERVICE_ENDPOINT')}/products/{product_id}").text)
            product_categories = json.loads(requests.get(f"{os.getenv('PRODUCT_SERVICE_ENDPOINT')}/products/categories", params = {"product": product_id}).text).get("results")
            cat = []
            for category in product_categories:
                category_te = json.loads(requests.get(f"{os.getenv('PRODUCT_SERVICE_ENDPOINT')}/categories/{category.get('category')}").text)
                cat.append(category_te)
            product["categories"] = cat
            product_images = json.loads(requests.get(f"{os.getenv('PRODUCT_SERVICE_ENDPOINT')}/products/images", params = {"product": product_id}).text).get("results")
            product["images"] = product_images
            product_comments = json.loads(requests.get(f"{os.getenv('COMMENT_SERVICE_ENDPOINT')}/comments", params = {"product_id": product_id}).text).get("results")
            for comment in product_comments:
                print(request.headers)
                comment_user = json.loads(requests.get(f"{os.getenv('USER_SERVICE_ENDPOINT')}/users/{comment.get('user_id')}").text)
                print(comment_user)
                # del comment_user["password"]
                # del comment_user["is_superuser"]
                # del comment_user["is_staff"]
                # del comment_user["is_active"]
                # del comment_user["date_joined"]
                comment["user"] = comment_user
            product["comments"] = product_comments
            
            return JsonResponse({
                "data": product
            }, status=status.HTTP_200_OK)
        except Exception as error:
            return JsonResponse({
                "message": str(error)
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)