from django.urls import path
from .views import _login, _register, _logout, _me, _list_create_address, _retrieve_update_destroy_address

urlpatterns = [
    path("users/auth/login", _login),
    path("users/auth/logout", _logout),
    path("users/auth/register", _register),
    path("users/auth/me", _me),
    path("addresses", _list_create_address),
    path("addresses/<int:pk>", _retrieve_update_destroy_address),
]