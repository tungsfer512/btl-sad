from rest_framework.response import Response
from rest_framework import status
import requests
import os
import json
from rest_framework.decorators import api_view
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema


user_id = openapi.Parameter(
    name="user",
    type=openapi.TYPE_INTEGER,
    in_=openapi.IN_QUERY,
    required=False
)
username = openapi.Parameter(
    name="username",
    type=openapi.TYPE_STRING,
    in_=openapi.IN_BODY,
    required=False
)
password = openapi.Parameter(
    name="password",
    type=openapi.TYPE_STRING,
    in_=openapi.IN_BODY,
    required=False
)
first_name = openapi.Parameter(
    name="first_name",
    type=openapi.TYPE_STRING,
    in_=openapi.IN_BODY,
    required=False
)
last_name = openapi.Parameter(
    name="last_name",
    type=openapi.TYPE_STRING,
    in_=openapi.IN_BODY,
    required=False
)
email = openapi.Parameter(
    name="email",
    type=openapi.TYPE_STRING,
    in_=openapi.IN_BODY,
    required=False
)
receiver_name = openapi.Parameter(
    name="receiver_name",
    type=openapi.TYPE_STRING,
    in_=openapi.IN_BODY,
)
receiver_phone = openapi.Parameter(
    name="receiver_phone",
    type=openapi.TYPE_STRING,
    in_=openapi.IN_BODY,
)
city = openapi.Parameter(
    name="city",
    type=openapi.TYPE_STRING,
    in_=openapi.IN_BODY,
)
town = openapi.Parameter(
    name="town",
    type=openapi.TYPE_STRING,
    in_=openapi.IN_BODY,
)
street = openapi.Parameter(
    name="street",
    type=openapi.TYPE_STRING,
    in_=openapi.IN_BODY,
)
address = openapi.Parameter(
    name="address",
    type=openapi.TYPE_STRING,
    in_=openapi.IN_BODY,
)
user = openapi.Parameter(
    name="user",
    type=openapi.TYPE_INTEGER,
    in_=openapi.IN_BODY,
    required=False
)

user_login_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT, 
    properties={
        'username': username,
        'password': password,
    }
)

user_register_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT, 
    properties={
        "username": username,
        "password": password,
        "first_name": first_name,
        "last_name": last_name,
        "email": email
    }
)

user_put_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT, 
    properties={
        "password": password,
        "first_name": first_name,
        "last_name": last_name,
        "email": email
    }
)

address_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT, 
    properties= {
        "receiver_name": receiver_name,
        "receiver_phone": receiver_phone,
        "city": city,
        "town": town,
        "street": street,
        "address": address,
        "user": user
    }
)

@swagger_auto_schema(
    method="post",
    request_body=user_login_schema
)
@api_view(("POST", ))
def _login(request, *args, **kwargs):
    if request.method == "POST":
        try:
            data = request.data
            print(data)
            response = requests.post(f"{os.getenv('USER_SERVICE_ENDPOINT')}/users/auth/login", data = data)
            token = json.loads(response.text)
            if response.status_code == 200:
                return Response(token, status=status.HTTP_200_OK)
            else:
                return Response(token, status=status.HTTP_400_BAD_REQUEST)
        except Exception as error:
            return Response({
                "message": str(error)
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@swagger_auto_schema(
    method="post",
    request_body=user_register_schema
)
@api_view(("POST", ))
def _register(request, *args, **kwargs):
    if request.method == "POST":
        try:
            data = request.data
            print(data)
            response = requests.post(f"{os.getenv('USER_SERVICE_ENDPOINT')}/users/auth/register", json = data)
            code = response.status_code
            if code == 201:
                user = json.loads(response.text)
                return Response(user, status=status.HTTP_200_OK)
            else:
                return Response(response.text, status=status.HTTP_400_BAD_REQUEST)
        except Exception as error:
            return Response({
                "message": str(error)
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(("POST", ))
def _logout(request, *args, **kwargs):
    if request.method == "POST":
        try:
            headers = request.headers
            print(headers)
            user = json.loads(requests.post(f"{os.getenv('USER_SERVICE_ENDPOINT')}/users/auth/logout", headers = headers).text)
            return Response(user, status=status.HTTP_200_OK)
        except Exception as error:
            return Response({
                "message": str(error)
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@swagger_auto_schema(
    method="put",
    request_body=user_put_schema
)
@api_view(("GET", "PUT", ))
def _me(request, *args, **kwargs):
    if request.method == "GET":
        try:
            headers = request.headers
            print(headers)
            response = requests.get(f"{os.getenv('USER_SERVICE_ENDPOINT')}/users/me", headers = headers)
            user = json.loads(response.text)
            if response.status_code == 200:
                return Response(user, status=status.HTTP_200_OK)
            else:
                return Response(user, status=status.HTTP_400_BAD_REQUEST)
        except Exception as error:
            return Response({
                "message": str(error)
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    elif request.method == "PUT":
        try:
            headers = request.headers
            data = request.data
            response = requests.put(f"{os.getenv('USER_SERVICE_ENDPOINT')}/users/me", headers = headers, json=data)
            user = json.loads(response.text)
            if response.status_code == 200:
                return Response(user, status=status.HTTP_200_OK)
            else:
                return Response(user, status=status.HTTP_400_BAD_REQUEST)
        except Exception as error:
            return Response({
                "message": str(error)
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@swagger_auto_schema(
    method="get",
    manual_parameters=(user_id, )
)
@swagger_auto_schema(
    method="post",
    request_body= address_schema
)
@api_view(("GET", "POST", ))
def _list_create_address(request, *args, **kwargs):
    if request.method == "GET":
        try:
            headers = request.headers
            params = request.query_params
            print(params)
            response = requests.get(f"{os.getenv('USER_SERVICE_ENDPOINT')}/addresses", headers = headers, params=params)
            addresses = json.loads(response.text).get("results")
            if response.status_code == 200:
                return Response({"data": addresses}, status=status.HTTP_200_OK)
            else:
                return Response(addresses, status=status.HTTP_400_BAD_REQUEST)
        except Exception as error:
            return Response({
                "message": str(error)
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    else:
        try:
            headers = request.headers
            params = request.query_params
            print(params)
            data = request.data
            print(data)
            response = requests.post(f"{os.getenv('USER_SERVICE_ENDPOINT')}/addresses", headers = headers, params=params, json=data)
            address = json.loads(response.text)
            if response.status_code == 201:
                return Response(address, status=status.HTTP_201_CREATED)
            else:
                return Response({"data": addresses}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as error:
            return Response({
                "message": str(error)
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@swagger_auto_schema(
    method="put",
    request_body= address_schema
)
@api_view(("GET", "PUT", ))
def _retrieve_update_destroy_address(request, *args, **kwargs):
    if request.method == "GET":
        try:
            headers = request.headers
            params = request.query_params
            print(params)
            response = requests.get(f"{os.getenv('USER_SERVICE_ENDPOINT')}/addresses/{kwargs.get('pk')}", headers = headers)
            addresses = json.loads(response.text)
            if response.status_code == 200:
                return Response({"data": addresses}, status=status.HTTP_200_OK)
            else:
                return Response(addresses, status=status.HTTP_400_BAD_REQUEST)
        except Exception as error:
            return Response({
                "message": str(error)
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    elif request.method == "PUT":
        try:
            headers = request.headers
            data = request.data
            if data.get("user", None) == None:
                data["user"] = int(kwargs.get('pk'))
            print(data)
            response = requests.put(f"{os.getenv('USER_SERVICE_ENDPOINT')}/addresses/{kwargs.get('pk')}", headers = headers, json=data)
            addresses = json.loads(response.text)
            if response.status_code == 200:
                return Response({"data": addresses}, status=status.HTTP_200_OK)
            else:
                return Response(addresses, status=status.HTTP_400_BAD_REQUEST)
        except Exception as error:
            return Response({
                "message": str(error)
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)