from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from rest_framework import status
import requests
import os
import json
from rest_framework.decorators import api_view
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema


user_id = openapi.Parameter(
    name="user_id",
    type=openapi.TYPE_INTEGER,
    in_=openapi.IN_BODY
)
product_id = openapi.Parameter(
    name="product_id",
    type=openapi.TYPE_INTEGER,
    in_=openapi.IN_BODY
)
quantity = openapi.Parameter(
    name="quantity",
    type=openapi.TYPE_INTEGER,
    in_=openapi.IN_BODY
)
update = openapi.Parameter(
    name="update",
    description="increase or decrease",
    type=openapi.TYPE_STRING,
    in_=openapi.IN_BODY
)
user_id_q = openapi.Parameter(
    name="user_id",
    type=openapi.TYPE_INTEGER,
    in_=openapi.IN_QUERY
)

cart_add_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT, 
    properties={
        'user_id': user_id,
        'product_id': product_id,
        'quantity': quantity,
    }
)
cart_update_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT, 
    properties={
        'update': update,
    }
)

wishlist_add_schema = openapi.Schema(
    type=openapi.TYPE_OBJECT, 
    properties={
        'user_id': user_id,
        'product_id': product_id,
    }
)


@swagger_auto_schema(
    method="get",
)
@swagger_auto_schema(
    method="post",
    request_body=cart_add_schema
)
@api_view(("GET", "POST", ))
def _list_create(request, *args, **kwargs):
    if request.method == "GET":
        try:
            params = request.query_params.dict()
            print(params)
            carts = json.loads(requests.get(f"{os.getenv('CART_SERVICE_ENDPOINT')}/carts", params = params).text).get("results")
            for cart in carts:
                product_id = cart.get("product_id")
                product = json.loads(requests.get(f"{os.getenv('PRODUCT_SERVICE_ENDPOINT')}/products/{product_id}").text)
                product_categories = json.loads(requests.get(f"{os.getenv('PRODUCT_SERVICE_ENDPOINT')}/products/categories", params = {"product": product_id}).text).get("results")
                cat = []
                for category in product_categories:
                    category_te = json.loads(requests.get(f"{os.getenv('PRODUCT_SERVICE_ENDPOINT')}/categories/{category.get('id')}").text)
                    cat.append(category_te)
                product["categories"] = cat
                product_images = json.loads(requests.get(f"{os.getenv('PRODUCT_SERVICE_ENDPOINT')}/products/images", params = {"product": product_id}).text).get("results")
                product["images"] = product_images
                product_comments = json.loads(requests.get(f"{os.getenv('COMMENT_SERVICE_ENDPOINT')}/comments", params = {"product_id": product_id}).text).get("results")
                product["comments"] = product_comments
                cart["product"] = product
            return JsonResponse({
                "data": carts
            }, status=status.HTTP_200_OK)
        except Exception as error:
            return JsonResponse({
                "message": str(error)
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    elif request.method == "POST":
        try:
            data = request.data
            response = requests.post(f"{os.getenv('CART_SERVICE_ENDPOINT')}/carts", json = data)
            cart = json.loads(response.text)
            if response.status_code == 201:
                return JsonResponse({
                    "data": cart
                }, status=status.HTTP_201_CREATED)
            else:
                return JsonResponse({
                    "data": cart
                }, status=status.HTTP_400_BAD_REQUEST)
        except Exception as error:
            return JsonResponse({
                "message": str(error)
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@swagger_auto_schema(
    method="put",
    request_body=cart_update_schema
)
@api_view(("put", "delete", ))
def _retrieve_update_destroy(request, *args, **kwargs):
    if request.method == "PUT":
        try:
            cart_id = int(kwargs.get("pk"))
            data = request.data
            response = requests.put(f"{os.getenv('CART_SERVICE_ENDPOINT')}/carts/{cart_id}", json = data)
            cart = json.loads(response.text)
            if response.status_code == 200:
                return JsonResponse({
                    "data": cart
                }, status=status.HTTP_200_OK)
            else:
                return JsonResponse({
                    "data": cart
                }, status=status.HTTP_400_BAD_REQUEST)
        except Exception as error:
            return JsonResponse({
                "message": str(error)
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    if request.method == "DELETE":
        try:
            cart_id = int(kwargs.get("pk"))
            cart = requests.delete(f"{os.getenv('CART_SERVICE_ENDPOINT')}/carts/{cart_id}").text
            return JsonResponse({
                "data": cart
            }, status=status.HTTP_200_OK)
        except Exception as error:
            return JsonResponse({
                "message": str(error)
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@swagger_auto_schema(
    method="post",
    request_body=wishlist_add_schema
)
@swagger_auto_schema(
    method="get", 
    manual_parameters=(user_id_q, )
)
@api_view(("GET", "POST", ))
def _list_create_wishlists(request, *args, **kwargs):
    if request.method == "GET":
        try:
            params = request.query_params.dict()
            print(params)
            wish_text = json.loads(requests.get(f"{os.getenv('CART_SERVICE_ENDPOINT')}/wishlists", params = params).text)
            wishlists = wish_text.get("results")
            count = wish_text.get("count")
            for wishlist in wishlists:
                product_id = wishlist.get("product_id")
                product = json.loads(requests.get(f"{os.getenv('PRODUCT_SERVICE_ENDPOINT')}/products/{product_id}").text)
                product_categories = json.loads(requests.get(f"{os.getenv('PRODUCT_SERVICE_ENDPOINT')}/products/categories", params = {"product": product_id}).text).get("results")
                cat = []
                for category in product_categories:
                    category_te = json.loads(requests.get(f"{os.getenv('PRODUCT_SERVICE_ENDPOINT')}/categories/{category.get('id')}").text)
                    cat.append(category_te)
                product["categories"] = cat
                product_images = json.loads(requests.get(f"{os.getenv('PRODUCT_SERVICE_ENDPOINT')}/products/images", params = {"product": product_id}).text).get("results")
                product["images"] = product_images
                product_comments = json.loads(requests.get(f"{os.getenv('COMMENT_SERVICE_ENDPOINT')}/comments", params = {"product_id": product_id}).text).get("results")
                product["comments"] = product_comments
                wishlist["product"] = product
            return JsonResponse({
                "count": count,
                "data": wishlists
            }, status=status.HTTP_200_OK)
        except Exception as error:
            return JsonResponse({
                "message": str(error)
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    elif request.method == "POST":
        try:
            data = request.data
            response = requests.post(f"{os.getenv('CART_SERVICE_ENDPOINT')}/wishlists", json = data)
            wishlist = json.loads(response.text)
            if response.status_code == 201 or response.status_code == 200:
                return JsonResponse({
                    "data": wishlist
                }, status=status.HTTP_201_CREATED)
            else:
                return JsonResponse({
                    "data": wishlist
                }, status=status.HTTP_400_BAD_REQUEST)
        except Exception as error:
            return JsonResponse({
                "message": str(error)
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(("DELETE", ))
def _retrieve_update_destroy_wishlists(request, *args, **kwargs):
    if request.method == "DELETE":
        try:
            wishlist_id = int(kwargs.get("pk"))
            wishlist = requests.delete(f"{os.getenv('CART_SERVICE_ENDPOINT')}/wishlists/{wishlist_id}").text
            return JsonResponse({
                "data": wishlist
            }, status=status.HTTP_200_OK)
        except Exception as error:
            return JsonResponse({
                "message": str(error)
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)