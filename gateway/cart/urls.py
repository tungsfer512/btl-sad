from django.urls import path
from .views import _list_create, _retrieve_update_destroy, _list_create_wishlists, _retrieve_update_destroy_wishlists

urlpatterns = [
    path("carts", _list_create),
    path("carts/<int:pk>", _retrieve_update_destroy),
    path("wishlists", _list_create_wishlists),
    path("wishlists/<int:pk>", _retrieve_update_destroy_wishlists),
]