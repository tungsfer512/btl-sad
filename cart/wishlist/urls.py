from django.urls import path
from wishlist.views import *
urlpatterns = [
    path('wishlists', ListCreateWishlistView.as_view()),
    path('wishlists/<int:pk>', RetrieveUpdateDestroyWishlistView.as_view()),
]