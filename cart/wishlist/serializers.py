from rest_framework import serializers
from wishlist.models import Wishlist

class WishlistSerializer(serializers.ModelSerializer):
    class Meta:
        model = Wishlist
        fields = (
            'id', 
            'user_id', 
            'product_id',
            'created',
        )
        extra_kwargs = {
            "created": {"required": False, "allow_null": True},
        }