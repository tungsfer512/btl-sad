from django.db import models
from django.utils import timezone

# Create your models here.

class Wishlist(models.Model):
    
    user_id = models.IntegerField(default=1)
    product_id = models.IntegerField(default=1)
    created = models.DateTimeField(null=True, default=timezone.now)
    
    class Meta:
        db_table = 'wishlist'