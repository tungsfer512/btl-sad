from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from config.views import ResultsSetPagination
from .models import *
from .serializers import *

class ListCreateWishlistView(ListCreateAPIView):
    queryset = Wishlist.objects.all().order_by("id")
    serializer_class = WishlistSerializer
    pagination_class = ResultsSetPagination
    filterset_fields = ["user_id", "product_id", ]
    search_fields = ["user_id", "product_id", ]
    def create(self, request, *args, **kwargs):
        try:
            data = request.data
            wishlist = self.get_queryset().filter(user_id=data.get(
                "user_id"), product_id=data.get("product_id")).first()
            if wishlist != None:
                return Response("Wishlist already existed", status=status.HTTP_200_OK)
            else:
                serializer = WishlistSerializer(data=data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Exception as error:
            return Response(str(error), status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class RetrieveUpdateDestroyWishlistView(RetrieveUpdateDestroyAPIView):
    queryset = Wishlist.objects.all().order_by("id")
    serializer_class = WishlistSerializer