from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from config.views import ResultsSetPagination
from .models import *
from .serializers import *

class ListCreateCartView(ListCreateAPIView):
    queryset = Cart.objects.all().order_by("id")
    serializer_class = CartSerializer
    pagination_class = ResultsSetPagination
    filterset_fields = ["user_id", "product_id", ]
    search_fields = ["user_id", "product_id", ]
    def create(self, request, *args, **kwargs):
        try:
            data = request.data
            print(type(request.data.get("user_id")))
            print(type(request.data.get("product_id")))
            print(type(request.data.get("quantity")))
            cart = self.get_queryset().filter(user_id=data.get(
                "user_id"), product_id=data.get("product_id")).first()
            if cart != None:
                cart.quantity = cart.quantity + data.get("quantity", 1)
                serializer = CartSerializer(cart, data=data)
            else:
                serializer = CartSerializer(data=data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Exception as error:
            return Response(str(error), status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class RetrieveUpdateDestroyCartView(RetrieveUpdateDestroyAPIView):
    queryset = Cart.objects.all().order_by("id")
    serializer_class = CartSerializer
    def update(self, request, *args, **kwargs):
        try:
            data = request.data
            path_variable = kwargs
            cart = self.get_queryset().filter(id=path_variable.get("pk")).first()
            if cart != None:
                if data.get("update") == "increase":
                    cart.quantity = cart.quantity + 1
                if data.get("update") == "decrease":
                    cart.quantity = cart.quantity - 1
                    if cart.quantity == 0:
                        cart.delete()
                        return Response(CartSerializer(cart).data, status=status.HTTP_200_OK)
                cart.save()
                return Response(CartSerializer(cart).data, status=status.HTTP_200_OK)
            else:
                return Response("Cart not found!", status=status.HTTP_404_NOT_FOUND)
        except Exception as error:
            return Response(str(error), status=status.HTTP_500_INTERNAL_SERVER_ERROR)
