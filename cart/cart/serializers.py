from rest_framework import serializers
from .models import Cart

class CartSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cart
        fields = "__all__"
        extra_kwargs = {
            "created": {"required": False, "allow_null": True},
            "updated": {"required": False, "allow_null": True},
            "quantity": {"required": False, "allow_null": True},
        }

class CartUpdateSerializer(serializers.ModelSerializer):
    update = serializers.CharField()
    class Meta:
        model = Cart
        fields = (
            "update",
        )