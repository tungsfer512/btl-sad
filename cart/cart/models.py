from django.db import models
from django.utils import timezone

class Cart(models.Model):
    user_id = models.IntegerField(default=1)
    product_id = models.IntegerField(default=1)
    quantity = models.IntegerField(default=1)
    created = models.DateTimeField(null=True, blank=True, default=timezone.now)
    updated = models.DateTimeField(null=True, blank=True, default=timezone.now)
    class Meta:
        db_table = 'cart'