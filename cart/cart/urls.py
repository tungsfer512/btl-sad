from django.urls import path
from .views import *

urlpatterns = [
    path('carts', ListCreateCartView.as_view()),
    path('carts/<int:pk>', RetrieveUpdateDestroyCartView.as_view()),
]