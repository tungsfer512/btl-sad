from django.db import models
from django.utils import timezone

# Create your models here.
class Shipment(models.Model):
    methods = (
        ("express", "express"),
        ("fast", "fast"),
        ("normal", "normal"),
    )
    statuses = (
        ("waiting", "waiting"), 
        ("packed", "packed"), 
        ("picked_up", "picked_up"), 
        ("shipped", "shipped"), 
    )
    
    user_id = models.IntegerField(default=1)
    payment_id = models.IntegerField(default=1)
    address_id = models.IntegerField(default=1)
    shipment_method = models.CharField(max_length=20, choices=methods, default="normal")
    cost = models.FloatField()
    shipment_status = models.CharField(max_length=20, choices=statuses, default="waiting")
    note = models.TextField()
    created = models.DateTimeField(null=True, blank=True, default=timezone.now)
    updated = models.DateTimeField(null=True, blank=True, default=timezone.now)
    
    class Meta:
        db_table = "shipment"