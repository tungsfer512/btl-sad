from rest_framework import serializers
from .models import *

class ShipmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shipment
        fields = "__all__"
        extra_kwargs = {
            "created": {"required": False, "allow_null": True},
            "updated": {"required": False, "allow_null": True},
        }