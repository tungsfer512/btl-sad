from django.urls import path
from .views import *

urlpatterns = [
    path("shipments", ListCreateShipmentView.as_view()),
    path("shipments/<int:pk>", RetrieveUpdateDestroyShipmentView.as_view()),
]