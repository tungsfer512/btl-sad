from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from config.views import ResultsSetPagination
from .models import *
from .serializers import *

class ListCreateShipmentView(ListCreateAPIView):
    queryset = Shipment.objects.all().order_by("id")
    serializer_class = ShipmentSerializer
    pagination_class = ResultsSetPagination
    filterset_fields = ["shipment_method", "shipment_status", "user_id", ]
    search_fields = ["payment_id", "shipment_method", "shipment_status", "note", ]

class RetrieveUpdateDestroyShipmentView(RetrieveUpdateDestroyAPIView):
    queryset = Shipment.objects.all().order_by("id")
    serializer_class = ShipmentSerializer