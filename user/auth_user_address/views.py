from config.views import ResultsSetPagination
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from .models import *
from .serializers import *

class ListCreateAuthUserAddressesView(ListCreateAPIView):
    queryset = AuthUserAddresses.objects.all().order_by("id")
    serializer_class = AuthUserAddressesSerializer
    pagination_class = ResultsSetPagination
    filterset_fields = ["city", "town", "street", "address", "user", ]
    search_fields = ["receiver_name", "receiver_phone", "city", "town", "street", "address", "user", ]

class RetrieveUpdateDestroyAuthUserAddressesView(RetrieveUpdateDestroyAPIView):
    queryset = AuthUserAddresses.objects.all()
    serializer_class = AuthUserAddressesSerializer