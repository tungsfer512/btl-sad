from django.db import models
from auth_user.models import AuthUser

# Create your models here.
class AuthUserAddresses(models.Model):
    
    receiver_name = models.TextField()
    receiver_phone = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    town = models.CharField(max_length=255)
    street = models.CharField(max_length=255, null=True)
    address = models.TextField()
    user = models.ForeignKey(AuthUser, on_delete=models.CASCADE)
    
    class Meta:
        db_table = 'auth_user_addresses'