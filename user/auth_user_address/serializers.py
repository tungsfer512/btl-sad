from rest_framework import serializers
from auth_user_address.models import AuthUserAddresses

class AuthUserAddressesSerializer(serializers.ModelSerializer):
  
  class Meta:
    model = AuthUserAddresses
    fields = "__all__"
    extra_kwargs = {
            "street": {"required": False, "allow_null": True},
            "town": {"required": False, "allow_null": True},
        }