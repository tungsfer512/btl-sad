from django.urls import path
from .views import *

urlpatterns = [
    path('addresses', ListCreateAuthUserAddressesView.as_view()),
    path('addresses/<int:pk>', RetrieveUpdateDestroyAuthUserAddressesView.as_view())
]
