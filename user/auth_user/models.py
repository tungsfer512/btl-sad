from django.db import models
from django.utils import timezone

# Create your models here.
class AuthUser(models.Model):

    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(null=True, default=timezone.now)
    is_superuser = models.BooleanField(default=False, null=True)
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150, null=True)
    last_name = models.CharField(max_length=150, null=True)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField(default=False, null=True)
    is_active = models.BooleanField(default=True, null=True)
    date_joined = models.DateTimeField(null=True, default=timezone.now)


    class Meta:
        managed = False
        db_table = 'auth_user'

class AuthtokenToken(models.Model):
    key = models.CharField(primary_key=True, max_length=40)
    created = models.DateTimeField(null=True, default=timezone.now)
    user = models.ForeignKey(AuthUser, on_delete=models.CASCADE)

    class Meta:
        managed = False
        db_table = 'authtoken_token'