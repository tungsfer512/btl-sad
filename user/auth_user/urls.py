from django.urls import path
from .views import *

urlpatterns = [
    path('users', ListCreateAuthUserView.as_view()),
    path('users/<int:pk>', RetrieveUpdateDestroyAuthUserView.as_view()),
    path('users/me', CurrentUserView.as_view()),
    path('users/auth/login', LoginView.as_view()),
    path('users/auth/logout', LogoutView.as_view()),
    path('users/auth/register', RegisterView.as_view()),
    path('users/token/<str:pk>', TokenView.as_view()),
]
