from config.views import ResultsSetPagination
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth.hashers import make_password, check_password
import uuid
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, CreateAPIView
from .models import *
from .serializers import *

class ListCreateAuthUserView(ListCreateAPIView):
    queryset = AuthUser.objects.all().order_by("id")
    serializer_class = AuthUserSerializer
    pagination_class = ResultsSetPagination
    filterset_fields = ["is_superuser", "is_staff", "is_active", ]
    search_fields = ["username", "first_name", "last_name", "email", ]

class RetrieveUpdateDestroyAuthUserView(RetrieveUpdateDestroyAPIView):
    queryset = AuthUser.objects.all().order_by("id")
    serializer_class = AuthUserUpdateSerializer

class CurrentUserView(RetrieveUpdateDestroyAPIView):
    queryset = AuthUser.objects.all().order_by("id")
    serializer_class = CurrentUserSerializer
    
    def retrieve(self, request, *args, **kwargs):
        try:
            token = request.headers['Authorization'].split(' ')[1]
            current_user_token = AuthtokenToken.objects.all().filter(key=token).first()
            if current_user_token != None:
                current_user = current_user_token.user
                return Response(AuthUserSerializer(current_user).data, status=status.HTTP_200_OK)
            else:
                return Response("Token not found!", status=status.HTTP_404_NOT_FOUND)
        except Exception as err:
            return Response(str(err), status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    def update(self, request, *args, **kwargs):
        try:
            token = request.headers['Authorization'].split(' ')[1]
            data = request.data
            current_user_token = AuthtokenToken.objects.all().filter(key=token).first()
            if current_user_token != None:
                current_user = current_user_token.user
                data["password"] = make_password(data.get("password"))
                data["username"] = current_user.username
                serializer = AuthUserSerializer(current_user, data=data)
                if serializer.is_valid():
                    serializer.save()
                else:
                    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
                return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response("Token not found!", status=status.HTTP_404_NOT_FOUND)
        except Exception as err:
            return Response(str(err), status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    def destroy(self, request, *args, **kwargs):
        try:
            token = request.headers['Authorization'].split(' ')[1]
            current_user_token = AuthtokenToken.objects.all().filter(key=token).first()
            if current_user_token != None:
                current_user = current_user_token.user
                current_user.delete()
                return Response("Delete user succefully!", status=status.HTTP_204_NO_CONTENT)
            else:
                return Response("Token not found!", status=status.HTTP_404_NOT_FOUND)
        except Exception as err:
            return Response(str(err), status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class LoginView(CreateAPIView):
    queryset = AuthUser.objects.all().order_by("id")
    serializer_class = LoginSerializer
    
    def create(self, request, *args, **kwargs):
        try:
            data = request.data
            user = AuthUser.objects.all().filter(username=data.get("username")).first()
            is_user = check_password(data.get("password"), user.password)
            if user != None and is_user == True:
                token = AuthtokenToken.objects.all().filter(user=user).first()
                user.is_active = True
                user.save()
                if token != None:
                    token.delete()
                serializer = AuthtokenTokenSerializer(data = {
                    "key": str(uuid.uuid4()),
                    "user": user.id
                })
                if serializer.is_valid():
                    serializer.save()
                else:
                    print(serializer.errors)
                print(serializer.data)
                return Response(serializer.data.get("key"), status=status.HTTP_200_OK)
            else:
                return Response("Unable to log in with provided credentials!", status=status.HTTP_400_BAD_REQUEST)
        except Exception as err:
            return Response(str(err), status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class RegisterView(CreateAPIView):
    queryset = AuthUser.objects.all().order_by("id")
    serializer_class = RegisterSerializer
    
    def create(self, request, *args, **kwargs):
        try:
            data = request.data
            user = AuthUser.objects.all().filter(username=data.get("username"), password=data.get("password")).first()
            if user == None:
                data["password"] = make_password(data.get("password"))
                data["is_superuser"] = False
                data["is_staff"] = False
                data["is_active"] = False
                serializer = AuthUserSerializer(data=data)
                if serializer.is_valid():
                    serializer.save()
                    return Response(serializer.data, status=status.HTTP_201_CREATED)
                else:
                    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response("Username already existed!", status=status.HTTP_400_BAD_REQUEST)
        except Exception as err:
            return Response(str(err), status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class LogoutView(CreateAPIView):
    serializer_class = LogoutSerializer
    queryset = AuthUser.objects.all().order_by("id")
    
    def create(self, request, *args, **kwargs):
        try:
            token = request.headers['Authorization'].split(' ')[1]
            print(token)
            current_user_token = AuthtokenToken.objects.all().filter(key=token).first()
            if current_user_token != None:
                current_user = current_user_token.user
                data = {}
                data['is_active'] = False
                serializer = AuthUserSerializer(current_user, data=data)
                if serializer.is_valid():
                    serializer.save()
                    current_user_token.delete()
                else:
                    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
                return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response("Token not found!", status=status.HTTP_404_NOT_FOUND)
        except Exception as err:
            return Response(str(err), status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        
class TokenView(RetrieveUpdateDestroyAPIView):
    queryset=AuthtokenToken.objects.all()
    serializer_class=AuthtokenTokenSerializer