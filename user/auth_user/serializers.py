from rest_framework import serializers
from .models import *


class AuthUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = AuthUser
        fields = "__all__"
        
        extra_kwargs = {
            "password": {"required": False, "allow_null": True},
            "username": {"required": False, "allow_null": True},
            "email": {"required": False, "allow_null": True},
        }

class AuthtokenTokenSerializer(serializers.ModelSerializer):

    class Meta:
        model = AuthtokenToken
        fields = "__all__"

class LoginSerializer(serializers.ModelSerializer):
    class Meta:
        model = AuthUser
        fields = (
            "username",
            "password",
        )

class LogoutSerializer(serializers.ModelSerializer):
    class Meta:
        model = AuthUser
        fields = ()

class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = AuthUser
        fields = (
            "username",
            "password",
            "first_name",
            "last_name",
            "email",
        )

class CurrentUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = AuthUser
        fields = (
            "id",
            "password",
            "first_name",
            "last_name",
            "email",
        )
class AuthUserUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = AuthUser
        fields = (
            "id",
            "password",
            "first_name",
            "last_name",
            "email",
        )