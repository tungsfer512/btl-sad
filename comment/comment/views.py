from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from config.views import ResultsSetPagination
from rest_framework.response import Response
from rest_framework import status
from .models import *
from .serializers import *
from .views import *

def add_comment_files(comment_id, images, videos):
    comment = Comment.objects.all().filter(id = comment_id).first()
    Image.objects.all().filter(comment=comment).delete()
    Video.objects.all().filter(comment=comment).delete()
    for image in images:
        Image(comment = comment, image = image).save()
    for video in videos:
        Video(comment = comment, video = video).save()

class ListCreateCommentView(ListCreateAPIView):
    model = Comment
    queryset = model.objects.all().order_by("id")
    serializer_class = CommentSerializer
    pagination_class = ResultsSetPagination
    filterset_fields = ["user_id", "order_id", "product_id", "rating"]
    search_fields = ["content", ]
    
    def list(self, request, *args, **kwargs):
        comments =  super().list(request, *args, **kwargs).data
        print(comments)
        for comment in comments.get("results"):
            images = ImageSerializer(Image.objects.all().order_by("id").filter(comment=comment.get("id")), many=True).data
            comment["images"] = images
            videos = VideoSerializer(Video.objects.all().order_by("id").filter(comment=comment.get("id")), many=True).data
            comment["videos"] = videos
        return Response(comments, status=status.HTTP_201_CREATED)
    
    def create(self, request, *args, **kwargs):
        images = request.FILES.getlist("images")
        if request.FILES.getlist("images") == None:
            images = []
        videos = request.FILES.getlist("videos")
        if request.FILES.getlist("videos") == None:
            videos = []
        print(images, videos)
        if request.data.get("images", None):
            del request.data["images"]
        if request.data.get("videos", None):
            del request.data["videos"]
        serializer_data = super().create(request, *args, **kwargs)
        print(serializer_data.data)
        add_comment_files(serializer_data.data.get("id"), images, videos)
        return Response(serializer_data.data, status=status.HTTP_201_CREATED)


class RetrieveUpdateDestroyCommentView(RetrieveUpdateDestroyAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    
    def retrieve(self, request, *args, **kwargs):
        comment =  super().retrieve(request, *args, **kwargs).data
        print(comment)
        images = ImageSerializer(Image.objects.all().order_by("id").filter(comment=comment.get("id")), many=True).data
        comment["images"] = images
        videos = VideoSerializer(Video.objects.all().order_by("id").filter(comment=comment.get("id")), many=True).data
        comment["videos"] = videos
        return Response(comment, status=status.HTTP_201_CREATED)
    
    def update(self, request, *args, **kwargs):
        images = request.FILES.getlist("images")
        if request.FILES.getlist("images") == None:
            images = []
        videos = request.FILES.getlist("videos")
        if request.FILES.getlist("videos") == None:
            videos = []
        print(images, videos)
        if request.data.get("images", None):
            del request.data["images"]
        if request.data.get("videos", None):
            del request.data["videos"]
        serializer_data = super().update(request, *args, **kwargs)
        print(serializer_data.data)
        add_comment_files(serializer_data.data.get("id"), images, videos)
        return Response(serializer_data.data, status=status.HTTP_200_OK)