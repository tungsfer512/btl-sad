from rest_framework import serializers
from comment.models import *

class CommentSerializer(serializers.ModelSerializer):
    
    images = serializers.ListField(child=serializers.ImageField(required=False, allow_null=True), required=False, allow_null=True)
    videos = serializers.ListField(child=serializers.FileField(required=False, allow_null=True), required=False, allow_null=True)
    
    class Meta:
        model = Comment
        fields = (
            "id",
            "user_id",
            "order_id",
            "product_id",
            "rating",
            "content",
            "created",
            "updated",
            "images",
            "videos",
        )
        extra_kwargs = {
            "content": {"required": False, "allow_null": True},
            "created": {"required": False, "allow_null": True},
            "updated": {"required": False, "allow_null": True},
            "images": {"required": False, "allow_null": True},
            "videos": {"required": False, "allow_null": True},
        }

class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model=Image
        fields = "__all__"

class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model=Video
        fields = "__all__"