from django.db import models
from django.utils import timezone

# Create your models here.
class Comment(models.Model):
    user_id = models.IntegerField(default=1)
    order_id = models.IntegerField(default=1)
    product_id = models.IntegerField(default=1)
    rating = models.FloatField()
    content = models.TextField(null=True, blank=True, default="")
    created = models.DateTimeField(null=True, blank=True, default=timezone.now)
    updated = models.DateTimeField(null=True, blank=True, default=timezone.now)
    class Meta:
        db_table = "comment_comment"

class Image(models.Model):
    image = models.ImageField(upload_to="images")
    comment = models.ForeignKey(Comment, on_delete=models.CASCADE)
    class Meta:
        db_table = "comment_images"

class Video(models.Model):
    video = models.FileField(upload_to="videos")
    comment = models.ForeignKey(Comment, on_delete=models.CASCADE)
    class Meta:
        db_table = "comment_videos"
