from django.urls import path
from .views import ListCreateCommentView, RetrieveUpdateDestroyCommentView

urlpatterns = [
    path("comments", ListCreateCommentView.as_view()),
    path("comments/<int:pk>", RetrieveUpdateDestroyCommentView.as_view()),
]