from rest_framework import serializers
from .models import *

class PaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Payment
        fields = "__all__"
        extra_kwargs = {
            "card_number": {"required": False, "allow_null": True},
            "created": {"required": False, "allow_null": True},
            "updated": {"required": False, "allow_null": True},
        }