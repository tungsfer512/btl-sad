from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from config.views import ResultsSetPagination
from .models import *
from .serializers import *

class ListCreatePaymentView(ListCreateAPIView):
    queryset = Payment.objects.all().order_by("id")
    serializer_class = PaymentSerializer
    pagination_class = ResultsSetPagination
    filterset_fields = ["payment_method", "payment_status", "user_id", ]
    search_fields = ["amount", "payment_method", "payment_status", "card_number", ]

class RetrieveUpdateDestroyPaymentView(RetrieveUpdateDestroyAPIView):
    queryset = Payment.objects.all().order_by("id")
    serializer_class = PaymentSerializer