from django.db import models
from django.utils import timezone

# Create your models here.
class Payment(models.Model):
    methods = (
        ("cash", "cash"),
        ("card", "card"),
    )
    statuses = (
        ("not_paid", "not_paid"), 
        ("paid", "paid"),
    )
    user_id = models.IntegerField(default=1)
    payment_method = models.CharField(max_length=20, choices=methods, default="cash")
    amount = models.FloatField()
    payment_status = models.CharField(max_length=20, choices=statuses, default="not_paid")
    card_number = models.CharField(max_length=20, null=True, blank=True)
    created = models.DateTimeField(null=True, blank=True, default=timezone.now)
    updated = models.DateTimeField(null=True, blank=True, default=timezone.now)
    
    class Meta:
        db_table = "payment"