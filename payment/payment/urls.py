from django.urls import path
from .views import *

urlpatterns = [
    path("payments", ListCreatePaymentView.as_view()),
    path("payments/<int:pk>", RetrieveUpdateDestroyPaymentView.as_view()),
]