from django.db import models
from django.utils import timezone
from product.models import Product

# Create your models here.
class Voucher(models.Model):
    units = (
        ("percent", "percent"),
        ("cash", "cash"),
    )
    discount = models.FloatField()
    condition = models.JSONField()
    unit = models.CharField(max_length=20, choices=units, default="percent")
    description = models.TextField()
    start = models.DateTimeField(null=True, default=timezone.now)
    end = models.DateTimeField(null=True, default=timezone.now)
    created = models.DateTimeField(null=True, default=timezone.now)
    updated = models.DateTimeField(null=True, default=timezone.now)
    
    class Meta:
        db_table = 'voucher'
        
class ProductVouchers(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    voucher = models.ForeignKey(Voucher, on_delete=models.CASCADE)
    
    class Meta:
        db_table = 'product_vouchers'
        