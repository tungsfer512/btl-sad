from rest_framework import serializers
from voucher.models import Voucher, ProductVouchers

class VoucherSerializer(serializers.ModelSerializer):
    
    product_ids = serializers.ListField(child = serializers.IntegerField(default=1))
    
    class Meta:
        model = Voucher
        fields = (
            "id",
            "discount",
            "condition",
            "unit",
            "description",
            "start",
            "end",
            "created",
            "updated",
            "product_ids",
        )
        extra_kwargs = {
            "created": {"required": False, "allow_null": True},
            "updated": {"required": False, "allow_null": True},
        }

class ProductVouchersSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductVouchers
        fields =  "__all__"
        