from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from config.views import ResultsSetPagination
from rest_framework.response import Response
from rest_framework import status
from .models import *
from .serializers import *

def add_voucher_products(voucher_id, product_ids):
    voucher = Voucher.objects.all().filter(id = voucher_id).first()
    products = Product.objects.all().filter(id__in = product_ids)
    ProductVouchers.objects.all().filter(voucher_id=voucher_id).delete()
    for product in products:
        ProductVouchers(product = product, voucher = voucher).save()

class ListCreateVoucherView(ListCreateAPIView):
    queryset = Voucher.objects.all().order_by("id")
    serializer_class = VoucherSerializer
    pagination_class = ResultsSetPagination
    filterset_fields = ["unit", ]
    search_fields = ["discount", "condition", "unit", "description", ]
    
    def create(self, request, *args, **kwargs):
        serializer_data = super().create(request, *args, **kwargs)
        print(serializer_data.data)
        if request.data.get("product_ids") != None:
            add_voucher_products(serializer_data.data.get("id"), [int(i) for i in request.data.get("product_ids")])
        return Response(serializer_data.data, status=status.HTTP_201_CREATED)

class RetrieveUpdateDestroyVoucherView(RetrieveUpdateDestroyAPIView):
    queryset = Voucher.objects.all().order_by("id")
    serializer_class = VoucherSerializer
    
    def update(self, request, *args, **kwargs):
        serializer_data = super().update(request, *args, **kwargs)
        print(serializer_data.data)
        if request.data.get("product_ids") != None:
            add_voucher_products(serializer_data.data.get("id"), [int(i) for i in request.data.get("product_ids")])
        return Response(serializer_data.data, status=status.HTTP_201_CREATED)
