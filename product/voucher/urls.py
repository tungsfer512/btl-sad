from django.urls import path
from voucher.views import *
urlpatterns = [
    path('vouchers', ListCreateVoucherView.as_view()),
    path('vouchers/<int:pk>', RetrieveUpdateDestroyVoucherView.as_view()),
]