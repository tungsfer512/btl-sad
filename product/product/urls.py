from django.urls import path
from .views import *

urlpatterns = [
    path("products", ListCreateProductView.as_view()),
    path("products/images", ListImagesView.as_view()),
    path("products/categories", ListCategoriesView.as_view()),
    path("products/<int:pk>", RetrieveUpdateDestroyProductView.as_view()),
]