from django.db import models
from django.utils import timezone
from category.models import Category
# Create your models here.

class Product(models.Model):
    statuses = (
        ("on", "on"),
        ("off", "off"),
    )
    title = models.TextField()
    quantity = models.IntegerField(default=1)
    price = models.FloatField()
    description = models.TextField()
    status = models.CharField(max_length=10, choices=statuses, default="on")
    created = models.DateTimeField(null=True, blank=True, default=timezone.now)
    updated = models.DateTimeField(null=True, blank=True, default=timezone.now)
    
    class Meta:
        db_table = 'product'
        
class ProductImages(models.Model):
    image = models.ImageField(upload_to="images", null=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    
    class Meta:
        db_table = 'product_images'
        
class ProductCategories(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    
    class Meta:
        db_table = 'product_categories'
        