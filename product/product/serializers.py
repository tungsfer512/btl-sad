from rest_framework import serializers
from .models import Product, ProductImages, ProductCategories

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = "__all__"
        extra_kwargs = {
            "created": {"required": False, "allow_null": True},
            "updated": {"required": False, "allow_null": True},
        }
class ProductImagesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductImages
        fields =  "__all__"
class ProductCategoriesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductCategories
        fields =  "__all__"

class ProductAddSerializer(serializers.ModelSerializer):
    
    images = serializers.ListField(child = serializers.ImageField(required=False, allow_null=True), required=False, allow_null=True)
    category_ids = serializers.ListField(child = serializers.IntegerField(default=1))
    
    class Meta:
        model = Product
        fields = (
            "id",
            "title",
            "quantity",
            "price",
            "description",
            "status",
            "created",
            "updated",
            "images",
            "category_ids",
        )
        extra_kwargs = {
            "images": {"required": False, "allow_null": True},
            "created": {"required": False, "allow_null": True},
            "updated": {"required": False, "allow_null": True},
        }