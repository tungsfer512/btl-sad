from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, ListAPIView
from config.views import ResultsSetPagination
from rest_framework.response import Response
from rest_framework import status
from .models import *
from .serializers import *

def add_product_images(product_id, images):
    product = Product.objects.all().filter(id = product_id).first()
    for image in images:
        ProductImages(product = product, image = image).save()

def add_product_categories(product_id, category_ids):
    ProductCategories.objects.all().filter(product_id=product_id).delete()
    categories = Category.objects.all().filter(id__in = category_ids)
    product = Product.objects.all().filter(id = product_id).first()
    for category in categories:
        ProductCategories(product = product, category = category).save()

class ListCreateProductView(ListCreateAPIView):
    queryset = Product.objects.all().order_by("id")
    serializer_class = ProductSerializer
    pagination_class = ResultsSetPagination
    filterset_fields = ["status", ]
    search_fields = ["title", "description", ]
    
    def list(self, request, *args, **kwargs):
        params = request.query_params.dict()
        priceStart = params.get("priceStart", None)
        priceEnd = params.get("priceEnd", None)
        if priceEnd != None:
            priceEnd = float(priceEnd)
            self.queryset = self.queryset.filter(price__lte=priceEnd)
        if priceStart != None:
            priceStart = float(priceStart)
            self.queryset = self.queryset.filter(price__gte=priceStart)
        return super().list(self, request, *args, **kwargs)
    
    def create(self, request, *args, **kwargs):
        serializer_data = super().create(request, *args, **kwargs)
        print(serializer_data.data)
        if request.FILES.getlist("images") != None:
            add_product_images(serializer_data.data.get("id"), request.FILES.getlist("images"))
        if request.data.get("category_ids", None) != None:
            add_product_categories(serializer_data.data.get("id"), [int(i) for i in request.data.get("category_ids")])
        return Response(serializer_data.data, status=status.HTTP_201_CREATED)

class RetrieveUpdateDestroyProductView(RetrieveUpdateDestroyAPIView):
    queryset = Product.objects.all().order_by("id")
    serializer_class = ProductSerializer
    
    def update(self, request, *args, **kwargs):
        serializer_data = super().update(request, *args, **kwargs)
        print(serializer_data.data)
        if request.FILES.getlist("images") != None:
            add_product_images(serializer_data.data.get("id"), request.FILES.getlist("images"))
        if request.data.get("category_ids") != None:
            add_product_categories(serializer_data.data.get("id"), [int(i) for i in request.data.get("category_ids")])
        return Response(serializer_data.data, status=status.HTTP_200_OK)

class ListImagesView(ListAPIView):
    queryset = ProductImages.objects.all().order_by("id")
    serializer_class = ProductImagesSerializer
    pagination_class = ResultsSetPagination
    filterset_fields = ["product", ]
    search_fields = ["product", "image", ]
class ListCategoriesView(ListAPIView):
    queryset = ProductCategories.objects.all().order_by("id")
    serializer_class = ProductCategoriesSerializer
    pagination_class = ResultsSetPagination
    filterset_fields = ["product", "category", ]
    search_fields = ["product", "category", ]