from django.db import models
from django.utils import timezone

# Create your models here.
class Category(models.Model):
    name = models.TextField()
    code = models.TextField()
    description = models.TextField()
    created = models.DateTimeField(null=True, default=timezone.now)
    updated = models.DateTimeField(null=True, default=timezone.now)
    
    class Meta:
        db_table = 'category'