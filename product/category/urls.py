from django.urls import path
from .views import *

urlpatterns = [
    path('categories', ListCreateCategoryView.as_view()),
    path('categories/<int:pk>', RetrieveUpdateDestroyCategoryView.as_view()),
]