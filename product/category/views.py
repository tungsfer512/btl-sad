from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from config.views import ResultsSetPagination
from .models import *
from .serializers import *

class ListCreateCategoryView(ListCreateAPIView):
    queryset = Category.objects.all().order_by("id")
    serializer_class = CategorySerializer
    pagination_class = ResultsSetPagination
    filterset_fields = ["name", "code", ]
    search_fields = ["name", "code", "description", ]

class RetrieveUpdateDestroyCategoryView(RetrieveUpdateDestroyAPIView):
    queryset = Category.objects.all().order_by("id")
    serializer_class = CategorySerializer