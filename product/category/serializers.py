from rest_framework import serializers
from .models import Category

class CategorySerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Category
        fields = "__all__"
        extra_kwargs = {
            "created": {"required": False, "allow_null": True},
            "updated": {"required": False, "allow_null": True},
        }