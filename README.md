# Website bán hàng

## Phân tích

**_Tự động hóa use case khách hàng đặt hàng và thanh toán_**

### Mô tả usecase

Khách hàng chọn sản phẩm trong giỏ hàng muốn thanh toán. Sau đó, khách hàng chọn phương thức thanh toán và nhập thông tin thanh toán. Nếu thanh toán thành công, hệ thống sẽ gửi thông tin giao hàng đến đơn vị vận chuyển và tạo đơn hàng.

### 1. Phân rã quy trình nghiệp vụ

1. Hệ thống nhận sự kiện tạo đơn hàng.
2. Lấy các thông tin được gửi đến(id của các sản phẩm cần thanh toán, số lượng từng sản phẩm, id người dùng)
3. Lấy thông tin người dùng từ User Service.
4. Kiểm tra thông tin người dùng.
5. Nếu người dùng không hợp lệ, kết thúc quy trình.
6. Gửi thông báo người dùng không hợp lệ.
7. Lấy thông tin sản phẩm từ Product Service.
8. Kiểm tra số lượng sản phẩm còn lại.
9. Nếu số lượng sản phẩm không đủ, kết thúc quy trình.
10. Gửi thông báo số lượng sản phẩm không đủ.
11. Người dùng chọn địa chỉ đã lưu hoặc thêm mới địa chỉ nhận hàng.
12. Hệ thống tính phí vận chuyển dựa trên số lượng sản phẩm và địa chỉ nhận hàng.
13. Hệ thống hiển thị thông tin đơn hàng (sản phẩm, giá, tổng tiền, phí vân chuyển,...).
14. Người dùng chọn phương thức thanh toán.
15. Nếu phương thức thanh toán là "Trả tiền mặt khi nhận hàng" thì chuyển đến bước 12 với trạng thái "Chưa thanh toán".
16. Người dùng nhập thông tin thanh toán.
17. Người dùng xác nhận thanh toán.
18. Hệ thống gửi yêu cầu đến Payment Service để xác thực thông tin thẻ và tạo thanh toán.
19. Nếu thanh toán không thành công, quay lại bước 17. Nếu thành công chuyển đến bước tiếp theo với trạng thái đơn hàng "Đã thanh toán".
20. Hệ thống gửi thông báo có đơn hàng đến nhân viên.
21. Nhân viên xem xét thông tin đơn hàng và nhấn nút "Xác nhận đơn hàng".
22. Hệ thống gửi thông tin giao hàng đến Shipment Service để tạo đơn vận chuyển.
23. Tạo đơn hàng thành công, hệ thống hiển thị thông báo thành công và chi tiết đơn hàng.

### 2. Lọc các hành động không phù hợp trong quá trình tự động hóa quy trình nghiệp vụ

Loại bỏ các hành động thủ công của con người và những hành động không phù hợp với quy trình tự động hóa. Các hành động bị loại bỏ:

1. Hệ thống nhận sự kiện tạo đơn hàng.
2. Lấy các thông tin được gửi đến(id của các sản phẩm cần thanh toán, số lượng từng sản phẩm, id người dùng)
3. Lấy thông tin người dùng từ User Service.
4. Kiểm tra thông tin người dùng.
5. Nếu người dùng không hợp lệ, kết thúc quy trình.
6. Gửi thông báo người dùng không hợp lệ.
7. Lấy thông tin sản phẩm từ Product Service.
8. Kiểm tra số lượng sản phẩm còn lại.
9. Nếu số lượng sản phẩm không đủ, kết thúc quy trình.
10. Gửi thông báo số lượng sản phẩm không đủ.
11. Người dùng chọn địa chỉ đã lưu hoặc thêm mới địa chỉ nhận hàng.
12. Hệ thống tính phí vận chuyển dựa trên số lượng sản phẩm và địa chỉ nhận hàng.
13. Hệ thống hiển thị thông tin đơn hàng (sản phẩm, giá, tổng tiền, phí vân chuyển,...).
14. Người dùng chọn phương thức thanh toán.
15. Nếu phương thức thanh toán là "Trả tiền mặt khi nhận hàng" thì chuyển đến bước 22 với trạng thái "Chưa thanh toán".
16. Người dùng nhập thông tin thanh toán.
17. Người dùng xác nhận thanh toán.
18. Hệ thống gửi yêu cầu đến Payment Service để xác thực thông tin thẻ và tạo thanh toán.
19. Nếu thanh toán không thành công, quay lại bước 17. Nếu thành công chuyển đến bước tiếp theo với trạng thái đơn hàng "Đã thanh toán".
20. ~~Hệ thống gửi thông báo có đơn hàng đến nhân viên.~~
21. ~~Nhân viên xem xét thông tin đơn hàng và nhấn nút "Xác nhận đơn hàng"~~.
22. Hệ thống gửi thông tin giao hàng đến Shipment Service để tạo đơn vận chuyển.
23. Tạo đơn hàng thành công, hệ thống hiển thị thông báo thành công và chi tiết đơn hàng.

### 3. Xác định các ứng viên dịch vụ thực thể

#### 3.1. Tách các ứng viên dịch vụ khả tri và bất khả tri

- Bất khả tri:
  - Nhận yêu cầu thanh toán
  - Tính phí vận chuyển
  - Gửi thông tin đến Payment Service
  - Gửi thông tin đến Shipment Service
  - Hiển thị thông báo thành công và chi tiết đơn hàng
- Khả tri: còn lại

#### 3.2. Ứng viên dịch vụ thực thể

![user entity](assets/3.1.png)
![product entity](assets/3.3.png)
![payment entity](assets/3.2.png)
![shipment entity](assets/3.4.png)

### 4. Xác định logic cụ thể của quy trình nghiệp vụ

- Logic dành riêng cho quy trình được tách thành lớp dịch vụ logic của riêng nó. Đối với một quy trình kinh doanh nhất định, loại logic này thường được nhóm thành một dịch vụ tác vụ hoặc một người tiêu dùng dịch vụ đóng vai trò là bộ điều khiển thành phần.

![specific logic](assets/4.1.png)

### 5. Xác định tài nguyên

| Entity   | Resource    |
| -------- | ----------- |
| User     | /users/     |
| Product  | /products/  |
| Payment  | /payments/  |
| Shipment | /shipments/ |

### 6. Liên kết các khả năng với các tài nguyên và phương thức

![user entity method](assets/5.1.png)
![product entity method](assets/5.3.png)
![payment entity method](assets/5.2.png)
![shipment entity method](assets/5.4.png)

### 7. Áp dụng hướng dịch vụ

### 8. Xác định ứng viên thành phần dịch vụ

![ung vien thanh phan dich vu](assets/8.1.png)

### 9. Phân tích yêu cầu của quy trình nghiệp vụ

### 10. Định nghĩa các ứng viên dịch vụ tiện ích

Hành động "gửi thông báo đặt hàng thành công" và 1 số thông báo khác được nhóm vào cùng 1 dịch vụ tiện ích là hành động "Gửi thông báo".

![Ứng viên dịch vụ tiện ích](assets/10.1.png)
![Ứng viên dịch vụ tiện ích](assets/10.2.png)

### 11. Định nghĩa các ứng viên vi dịch vụ

Để tách biệt các hành động xác minh với quy trinh, ta thiết lập một ứng cử viên vi dịch vụ có tên là "Verify Application", với một ứng cử viên năng lực dịch vụ "Verify" duy nhất

![Micro service](assets/11.1.png)

### 12. Áp dụng hướng dịch vụ

### 13. Xem lại các ứng viên thành phần dịch vụ

![layer](assets/13.1.png)

### 14. Xem lại định nghĩa các tài nguyên và nhóm các ứng viên khả năng dịch vụ

## Thiết kế

### 1. Thiết kế dịch vụ thực thể

![user-entity](assets/x-1.png)
![payment-entity](assets/x-2.png)
![product-entity](assets/x-3.png)
![shipment-entity](assets/x-4.png)

### 2. Thiết kế dịch vụ tiện ích

![notification-entity](assets/x-5.png)

## Công nghệ sử dụng

### 1. Công nghệ sử dụng

|Công nghệ sử dụng|Chức năng|
|-----------------|---------|
|Python|Ngôn ngữ sử dụng để code với Django và DRF để xây dựng microservices|
|Django|Xây dựng các microservices|
|Django Rest Framework|Xây dựng Restful APIs phù hợp với kiểu kiến trúc REST Service|
|Docker|Đóng gói các service, hỗ trợ triển khai|
|WebSocket|Kết nối web, app với backend để nhận, gửi các thông báo|

### 2. Mô tả API Gateway

- API Gateway là một lớp phần mềm trung gian giúp điều hướng các lệnh gọi yêu cầu HTTP đến từ các ứng dụng Máy khách đến Microservice cụ thể mà không trực tiếp hiển thị chi tiết Microservice cho Máy khách và trả về các phản hồi được tạo từ Microservice tương ứng.
- Ở đây ta tạo ra các api gateway để nhận dữ liệu từ người dùng, có thể truy cập đến các dịch vụ cụ thể để lấy dữ liệu rồi xử lý dữ liệu trả lại cho người dùng
- Tương tự ta vẫn sử dụng python và django cùng với djangorestframework nhưng ta cần có thêm 1 thư viện hỗ trợ là ```requests```
- Ở đây, ```requests``` được sử dụng để gửi yêu cầu HTTP giữa các dịch vụ khác nhau
- Khi xây dựng một API gateway, ```requests``` có thể được sử dụng để gọi các API endpoint của các microservice khác để lấy dữ liệu và trả về cho khách hàng. Điều này giúp tách biệt các dịch vụ nhỏ hơn và đơn giản hóa việc giao tiếp giữa chúng.
- Khi có yêu cầu từ phía client, API Gateway sẽ request đến User Service để xác thực thông tin của user trước khi cho phép user thực hiện các hành động khác. Nếu xác thực thành công, API Gateway sẽ request đến các microservice khác để thực hiện các hành động khác.

## Triển khai

### 1.1. Các cổng sử dụng

|Service|Port|Database Port|
|---|---|---|
|Ai Service|9111|5433|
|Cart Service|9112|5434|
|Comment Service|9113|5435|
|Order Service|9114|5436|
|Payment Service|9115|5437|
|Product Service|9116|5438|
|Shipment Service|9117|5439|
|User Service|9118|5440|
|Api Gateway|9999|5444|

### 1.2. Cấu trúc dự án

Bao gồm 8 microservices và 1 api gateway:

![tongquan](assets/tongquan.png)

- gateway: API Gateway
- ai: Service AI sử dụng để tìm kiếm sản phẩm bằng hình ảnh

![ai](assets/ai.png)

- cart: Giỏ hàng

![cart](assets/cart.png)

- comment: Feedback và đánh giá sản phẩm

![comment](assets/comment.png)

- order: Đặt hàng

![order](assets/order.png)

- payment: Thanh toán

![payment](assets/payment.png)

- product: Quản lý sản phẩm

![product](assets/product.png)

- shipment: Vận chuyển

![shipment](assets/shipment.png)

- user: Quản lý người dùng, đăng nhập, đăng ký

![user](assets/user.png)

### 1.3. Mô tả chi tiết

#### 1.4.1. AI Service

- Chức năng: Tìm kiếm sản phẩm bằng hình ảnh

#### 1.4.2. Cart Service

- Chức năng:
  - Thêm sản phẩm vào giỏ hàng
  - Cập nhật số lượng sản phẩm trong giỏ hàng
  - Xóa sản phẩm khỏi giỏ hàng
  - Thêm sản phẩm vào danh sách yêu thích
  - Xóa sản phẩm khỏi danh sách yêu thích

#### 1.4.3. Comment Service

- Chức năng:
  - Thêm đánh giá sản phẩm: rating, comment, hình ảnh, video
  - Chỉnh sửa đánh giá sản phẩm
  - Xóa đánh giá sản phẩm

#### 1.4.4. Order Service

- Chức năng:
  - Thêm đơn hàng
  - Cập nhật trạng thái đơn hàng
  - Hủy đơn hàng

#### 1.4.5. Payment Service

- Chức năng:
  - Thanh toán đơn hàng qua Paypal
  - Cập nhật trạng thái thanh toán

#### 1.4.6. Product Service

- Chức năng:
  - Thêm sản phẩm
  - Cập nhật sản phẩm
  - Xóa sản phẩm
  - Tìm kiếm sản phẩm
  - Lọc sản phẩm
  - Sắp xếp sản phẩm
  - Xem chi tiết sản phẩm
  - Thêm danh mục sản phẩm
  - Cập nhật danh mục sản phẩm
  - Xóa danh mục sản phẩm
  - Thêm voucher
  - Cập nhật voucher
  - Xóa voucher

#### 1.4.7. Shipment Service

- Chức năng:
  - Tính phí vận chuyển
  - Cập nhật trạng thái vận chuyển

#### 1.4.8. User Service

- Chức năng:
  - Đăng ký tài khoản
  - Đăng nhập
  - Xem thông tin tài khoản
  - Cập nhật thông tin tài khoản
  - Quản lý địa chỉ giao hàng
  - Quản lý danh sách người dùng

### 1.4. Preqrequisites

- Docker
- Docker Compose

### 1.5. Start

```bash
docker-compose up -d
```

### 1.6. Stop

```bash
docker-compose down
```
