from rest_framework import serializers
from .models import *

class Orderializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = "__all__"
        extra_kwargs = {
            "created": {"required": False, "allow_null": True},
            "updated": {"required": False, "allow_null": True},
        }
class OrderProductsSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderProducts
        fields =  "__all__"

class OrderAddSerializer(serializers.ModelSerializer):
    
    products = serializers.ListField(child = serializers.JSONField(required=False, allow_null=True), required=False, allow_null=True)
    
    class Meta:
        model = Order
        fields = "__all__"
        extra_kwargs = {
            "products": {"required": False, "allow_null": True},
            "created": {"required": False, "allow_null": True},
            "updated": {"required": False, "allow_null": True},
        }
