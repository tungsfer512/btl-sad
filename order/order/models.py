from django.db import models
from django.utils import timezone

# Create your models here.
class Order(models.Model):
    user_id = models.IntegerField(default=1)
    shipment_id = models.IntegerField(default=1)
    created = models.DateTimeField(null=True, blank=True, default=timezone.now)
    updated = models.DateTimeField(null=True, blank=True, default=timezone.now)
    
    class Meta:
        db_table = "order"

class OrderProducts(models.Model):
    
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    product_id = models.IntegerField(default=1)
    quantity = models.IntegerField(default=1)
    price = models.FloatField(null=True, blank=True)
    
    class Meta:
        db_table = "order_products"