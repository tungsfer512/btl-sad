from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, ListAPIView
from config.views import ResultsSetPagination
from rest_framework.response import Response
from django.core import serializers as ser
from rest_framework import status
import json
from .models import *
from .serializers import *

def add_order_products(order_id, products):
    order = Order.objects.all().filter(id = order_id).first()
    for product in products:
        OrderProducts(order = order, product_id = product["id"], quantity = product["quantity"], price = product['price']).save()

class ListCreateOrderView(ListCreateAPIView):
    queryset = Order.objects.all().order_by("id")
    serializer_class = OrderAddSerializer
    filterset_fields = ["shipment_id", "user_id", ]
    search_fields = ["note", ]
    
    def list(self, request, *args, **kwargs):
        orders = super().list(request, *args, **kwargs).data
        
        for order in orders.get("results"):
            order_products = OrderProducts.objects.all().filter(order = order.get("id"))
            order["products"] = list(order_products.values())
        
        return Response(orders, status=status.HTTP_201_CREATED)
    
    def create(self, request, *args, **kwargs):
        products = request.data.get("products")
        del request.data["products"]
        serializer_data = super().create(request, *args, **kwargs)
        print(serializer_data.data)
        if products != None:
            add_order_products(serializer_data.data.get("id"), products)
        return Response(serializer_data.data, status=status.HTTP_201_CREATED)

class RetrieveUpdateDestroyOrderView(RetrieveUpdateDestroyAPIView):
    queryset = Order.objects.all().order_by("id")
    serializer_class = OrderAddSerializer
    
    def retrieve(self, request, *args, **kwargs):
        order = super().retrieve(request, *args, **kwargs).data
        order_products = OrderProducts.objects.all().filter(order = order.get("id"))
        print(order_products)
        order["products"] = list(order_products.values())
        return Response(order, status=status.HTTP_200_OK)

class ListOrderProductsView(ListAPIView):
    queryset = OrderProducts.objects.all().order_by("id")
    serializer_class = OrderProductsSerializer
    pagination_class = ResultsSetPagination
    filterset_fields = ["order", ]
    search_fields = ["product_id", "order", "quantity", ]