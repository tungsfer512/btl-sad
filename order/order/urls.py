from django.urls import path
from .views import *

urlpatterns = [
    path("orders", ListCreateOrderView.as_view()),
    path("orders/products", ListOrderProductsView.as_view()),
    path("orders/<int:pk>", RetrieveUpdateDestroyOrderView.as_view()),
]